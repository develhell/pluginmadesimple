package com.develhell.icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class SmartyIcons
{
	public static final Icon SmartyFile = IconLoader.getIcon("/com/develhell/icons/smartyFileIcon.png");
}
